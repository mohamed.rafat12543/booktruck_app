import 'package:get_it/get_it.dart';
import 'package:booktruck_app/utils/connectivity_state.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerLazySingleton(() => ConnectivityManager());
}
